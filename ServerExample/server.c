#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

typedef int SOCKET;

int main(int argc, char *argv[]) {

	// listening socket
	SOCKET listenfd = 0;
	// socket for accepted client
	SOCKET connfd = 0;
	// Server Address structure
	struct sockaddr_in serv_addr;
	// Client Address structure
	struct sockaddr_in client_addr;

	// option 1 (set), needed for setsockopt()
	int option = 1;

	// File handle to output file
	FILE *file;

	// pointer into a Buffer
	void *pBuf = 0;
	// file data Buffer
	char *file_data;
	// path data Buffer
	char *file_path;

	// sizes of buffers
	int path_length;
	int fileSize;

	// variables for send/recv while loops
	int bytes_written;
	int remain_data;
	int res = 0;
	// size of sockackdr struct, for accept
	socklen_t sock_len;


	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	// Set Socket options to reuse Port and Address (no timeout)
	if (setsockopt(listenfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR),
			(char*) &option, sizeof(option)) < 0) {
		perror("setsockopt failed");
		close(listenfd);
		return EXIT_FAILURE;

	}
	// Fill addr struct
	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(5000);
	// Bind to Port and Interface
	bind(listenfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr));
	// Start recieving packets
	listen(listenfd, 10);
	sock_len = sizeof(struct sockaddr_in);
	while (1) {
		// Accept Connection
		connfd = accept(listenfd, (struct sockaddr*) &client_addr, &sock_len);

		fprintf(stdout, "Client connected --> %s\n",
				inet_ntoa(client_addr.sin_addr));

		path_length = 1;
		// Server loop, holds connection while not recieving and empty path
		while (path_length) {
			// Recieve length of the incoming data for the filepath
			res = recv(connfd, &path_length, sizeof(int), 0);
			if (!res || !path_length)
				break;

			fprintf(stdout, "Path size : %d\n", path_length);
			// Create a buffer for the path
			file_path = (char*) malloc(path_length);
			memset(file_path, 0, path_length);


			// Recieve the path data
			pBuf = file_path;
			remain_data = path_length;
			while (((res = recv(connfd, pBuf, remain_data, 0)) > 0)
					&& (remain_data > 0)) {
				fprintf(stdout, "Read %d bytes from client for path, remaining: %d\n", res, remain_data);

				pBuf += res;
				remain_data -= res;
				if (!remain_data)
					break;
			}

			// open the file
			file_path[path_length] = '\0';	// \0 for fopen (readline reads \n)
			fprintf(stdout, "Got path %s\n", file_path);
			file = fopen(file_path, "r");
			// on error send filesize 0 to client
			if (file == 0) {
				fileSize = 0;
				perror("Error opening file:");
				res = send(connfd, &fileSize, sizeof(int), 0);
								if (res < 0) {
									perror("error sending size 0");

									exit(EXIT_FAILURE);
								}
					free(file_path);
					file_path = 0;

			// else send the file
			} else {

				// get length of file
				fseek(file, 0L, SEEK_END);
				fileSize = ftell(file);
				fseek(file, 0L, SEEK_SET);

				fprintf(stdout, "File Size: \n%d bytes\n", fileSize);

				// create buffer;
				file_data = (char*) malloc(fileSize);

				res = fread(file_data, sizeof(char), fileSize, file);
				if (res < 1) {
					perror("Error reading file");
				} else {
					fprintf(stdout, "read %d bytes of file into buffer\n", res);
				}

				fclose(file);
				file = 0;


				res = send(connfd, &fileSize, sizeof(int), 0);
				if (res < 0) {
					fprintf(stderr, "Error on sending size --> %s",
							strerror(errno));

					exit(EXIT_FAILURE);
				}

				fprintf(stdout, "Server sent %d bytes for the size\n", res);

				pBuf = file_data;
				remain_data = fileSize;
				while (remain_data > 0) {
					bytes_written = write(connfd, pBuf, remain_data);
					if (bytes_written <= 0) {
						perror("error sending file");
					}

					remain_data -= bytes_written;
					pBuf += bytes_written;
					fprintf(stdout,
							"Server sent %d bytes from file, remaining %d\n",
							bytes_written, remain_data);
				}
				free(file_path);
				file_path = 0;
				free(file_data);
				file_data = 0;
			}
		}
		printf("Disconnected Client %s\n", inet_ntoa(client_addr.sin_addr));
		close(connfd);
		sleep(1);
	}
}
