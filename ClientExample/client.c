#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {
	int sockfd = 0;
	struct sockaddr_in serv_addr;
	char *fileName = 0;
	size_t pathSize = 0;
	int pathLength = 0;
	FILE *recvFile = 0;

	int res = 0;
	int bytes_written = 0;
	int remain_data = 0;
	int file_length = 0;
	char *file_data;
	void *pBuf = 0;
	if (argc != 2) {
		printf("\n Usage: %s <ip of server> \n", argv[0]);
		return 1;
	}


	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Error : Could not create socket \n");
		return 1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(5000);

	if (inet_pton(AF_INET, argv[1], &serv_addr.sin_addr) <= 0) {
		printf("\n inet_pton error occured\n");
		return 1;
	}

	if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))
			< 0) {
		printf("\n Error : Connect Failed \n");
		return 1;
	}
	pathLength = 1;
	while (pathLength) {
		printf("Give path:\n");
		pathLength = getline(&fileName, &pathSize, stdin);
		if (pathLength == 1) {
			if(recvFile)fclose(recvFile);
			if(fileName)free(fileName);
			if(file_data)free(file_data);
			fileName = 0;
			file_data = 0;
			recvFile = 0;
			break;
		}
		if (pathLength == -1) {
			perror("getline");
		}
		printf("Got path: %s\n", fileName);
		pathLength--;
		res = send(sockfd, &pathLength, sizeof(int), 0);
		if (res == -1) {
			perror("send path length");
		}

		pBuf = fileName;
		remain_data = pathLength;
		while (remain_data > 0) {
			bytes_written = send(sockfd, pBuf, remain_data, 0);
			if (bytes_written <= 0) {
				perror("error sending path");
			}

			remain_data -= bytes_written;
			pBuf += bytes_written;
			fprintf(stdout, "sent %d bytes from path, remaining %d\n",
					bytes_written, remain_data);
		}

		res = recv(sockfd, &file_length, sizeof(int), 0);
		if(res < 0 ) {
			perror("error recieving file length");
		}
		if (!file_length) {
			if(recvFile)fclose(recvFile);
			if(fileName)free(fileName);
			if(file_data)free(file_data);
			fileName = 0;
			file_data = 0;
			recvFile = 0;
			continue;
		}

		file_data = (char*) malloc(file_length);
		memset(file_data, 0, file_length);
		pBuf = file_data;
		remain_data = file_length;

		while (((res = recv(sockfd, pBuf, remain_data, 0)) > 0)
				&& (remain_data > 0)) {
			fprintf(stdout,
					"recv %d bytes from server for file remaining: %d \n", res,
					remain_data);

			pBuf += res;
			remain_data -= res;
			if (!remain_data)
				break;
		}
		fileName[pathLength] = '\0';
		recvFile = fopen(fileName, "w");
		if (!recvFile) {
			perror("error opening file");
		}
		res = fwrite(file_data, sizeof(char), file_length, recvFile);
		if (res < 0) {
			perror("error writing to file");

		}
		printf("Wrote %d bytes to Disk\n", res);
		if(recvFile)fclose(recvFile);
		if(fileName)free(fileName);
		if(file_data)free(file_data);
		fileName = 0;
		file_data = 0;
		recvFile = 0;
	}
	if(recvFile)fclose(recvFile);
	if(fileName)free(fileName);
	if(file_data)free(file_data);
	fileName = 0;
	file_data = 0;
	recvFile = 0;
	return 0;
}
